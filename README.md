# Repo for analysis of genitives across genres

Contains R code and data for running the analysis for recent papers.

- Grafmiller, Jason. 2018. When context shapes grammar: Stylistic flexibility in the English
genitive alternation. *ICL* 20. Cape Town, South Africa. July 3.
- Grafmiller, Jason. 2018. Variable cues vs. multiple grammars: Genre specificity in the English genitive alternation. *ICAME* 39. Tampere, Finland. May 31.
